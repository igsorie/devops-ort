variable "aws_region"{
   description = "AWS region"
   default = "us-east-1"
}

variable "aws_az"{
   description = "AWS AZ"
   default = "us-east-1a"
}