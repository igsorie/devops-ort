provider "aws"{
    region = var.aws_region
}

resource "aws_ecs_cluster" "cluster" {
  name = "cluster"
  setting {
    name  = "containerInsights"
    value = "enabled"
  }
}
#prueba
data "aws_ecs_task_definition" "orders-service-example" {
  task_definition = "${aws_ecs_task_definition.orders.family}"
}

data "aws_ecs_task_definition" "shipping-service-example" {
  task_definition = "${aws_ecs_task_definition.shipping.family}"
}

data "aws_ecs_task_definition" "products-service-example" {
  task_definition = "${aws_ecs_task_definition.products.family}"
}

data "aws_ecs_task_definition" "payments-service-example" {
  task_definition = "${aws_ecs_task_definition.payments.family}"
}


# ======================  TASKS =================== #
resource "aws_ecs_task_definition" "orders" {
  family                   = "ordersService"
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE", "EC2"]
  cpu                      = 1024
  memory                   = 4096
  container_definitions    = <<DEFINITION
  [
    {
      "name"      : "orders-service-example",
      "image"     : "registry.gitlab.com/igsorie/orders-service-example:latest",
      "cpu"       : 512,
      "memory"    : 2048,
      "essential" : true,
      "portMappings" : [
        {
          "containerPort" : 8080,
          "hostPort"      : 8080
        }
      ]
    }
  ]
  DEFINITION
}

resource "aws_ecs_task_definition" "products" {
  family                   = "productService"
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE", "EC2"]
  cpu                      = 1024
  memory                   = 4096
  container_definitions    = <<DEFINITION
  [
    {
      "name"      : "products-service-example",
      "image"     : "registry.gitlab.com/igsorie/products-service-example:latest",
      "cpu"       : 512,
      "memory"    : 2048,
      "essential" : true,
      "portMappings" : [
        {
          "containerPort" : 8080,
          "hostPort"      : 8080
        }
      ]
    }
  ]
  DEFINITION
}

resource "aws_ecs_task_definition" "shipping" {
  family                   = "shippingService"
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE", "EC2"]
  cpu                      = 1024
  memory                   = 4096
  container_definitions    = <<DEFINITION
  [
    {
      "name"      : "shipping-service-example",
      "image"     : "registry.gitlab.com/igsorie/shipping-service-example:latest",
      "cpu"       : 512,
      "memory"    : 2048,
      "essential" : true,
      "portMappings" : [
        {
          "containerPort" : 8080,
          "hostPort"      : 8080
        }
      ]
    }
  ]
  DEFINITION
}

resource "aws_ecs_task_definition" "payments" {
  family                   = "paymentService"
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE", "EC2"]
  cpu                      = 1024
  memory                   = 4096
  container_definitions    = <<DEFINITION
  [
    {
      "name"      : "payments-service-example",
      "image"     : "registry.gitlab.com/igsorie/payments-service-example:latest",
      "cpu"       : 512,
      "memory"    : 2048,
      "essential" : true,
      "portMappings" : [
        {
          "containerPort" : 8080,
          "hostPort"      : 8080
        }
      ]
    }
  ]
  DEFINITION
}


# ======================  SERVICES =================== #

resource "aws_ecs_service" "shippingService" {
  name             = "shippingService"
  cluster          = aws_ecs_cluster.cluster.id
  task_definition  = aws_ecs_task_definition.shipping.id
  desired_count    = 1
  launch_type      = "FARGATE"
  platform_version = "LATEST"

  network_configuration {
    assign_public_ip = true
    security_groups  = [aws_security_group.dev-vpc-sg.id]
    subnets          = [aws_subnet.vpc-dev-public-subnet-1.id]
  }
  lifecycle {
    ignore_changes = [task_definition]
  }
}

resource "aws_ecs_service" "productsService" {
  name             = "productsService"
  cluster          = aws_ecs_cluster.cluster.id
  task_definition  = aws_ecs_task_definition.products.id
  desired_count    = 1
  launch_type      = "FARGATE"
  platform_version = "LATEST"

  network_configuration {
    assign_public_ip = true
    security_groups  = [aws_security_group.dev-vpc-sg.id]
    subnets          = [aws_subnet.vpc-dev-public-subnet-1.id]
  }
  lifecycle {
    ignore_changes = [task_definition]
  }
}

resource "aws_ecs_service" "paymentService" {
  name             = "paymentService"
  cluster          = aws_ecs_cluster.cluster.id
  task_definition  = aws_ecs_task_definition.payments.id
  desired_count    = 1
  launch_type      = "FARGATE"
  platform_version = "LATEST"

  network_configuration {
    assign_public_ip = true
    security_groups  = [aws_security_group.dev-vpc-sg.id]
    subnets          = [aws_subnet.vpc-dev-public-subnet-1.id]
  }
  lifecycle {
    ignore_changes = [task_definition]
  }
}

resource "aws_ecs_service" "ordersService" {
  name             = "orderService"
  cluster          = aws_ecs_cluster.cluster.id
  task_definition  = aws_ecs_task_definition.orders.id
  desired_count    = 1
  launch_type      = "FARGATE"
  platform_version = "LATEST"

  network_configuration {
    assign_public_ip = true
    security_groups  = [aws_security_group.dev-vpc-sg.id]
    subnets          = [aws_subnet.vpc-dev-public-subnet-1.id]
  }
  lifecycle {
    ignore_changes = [task_definition]
  }
}
